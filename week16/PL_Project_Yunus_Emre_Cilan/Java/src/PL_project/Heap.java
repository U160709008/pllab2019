package PL_project;

import java.util.Vector;

public class Heap {

	private String[] heapArray;
	/** size of array **/
	private int maxSize;
	/** number of nodes in array **/
	private int heapSize;

	/** Constructor **/
	public Heap(int mx) {
		maxSize = mx;
		heapSize = 0;
		heapArray = new String[maxSize];
	}

	/** Check if heap is empty **/
	public boolean isEmpty() {
		return heapSize == 0;
	}

	/** Function to insert element **/
	public boolean insert(String ele) {
		if (heapSize + 1 == maxSize)
			return false;
		heapArray[heapSize++] = ele;
		int pos = heapSize;
		while (pos != 1 && ele.compareTo(heapArray[pos / 2]) < 0) {
			heapArray[pos] = heapArray[pos / 2];
			pos /= 2;
		}
		heapArray[pos] = ele;
		return true;
	}
	public boolean search(String data) {
		boolean test=false;
		for (int i = 0; i < heapArray.length-1; i++) {
			if(heapArray[i].equals(data)) {
				test =true;
				break;
				}
				else {
					test  = false;
				}
		}
		return test;
	}
	/** Function to print values **/
	public void displayHeap() {
		/* Array format */
		System.out.print("\nHeap array: ");
		for (int i = 1; i <= heapSize; i++)
			System.out.print(heapArray[i] + " ");
		System.out.println("\n");
	}

}