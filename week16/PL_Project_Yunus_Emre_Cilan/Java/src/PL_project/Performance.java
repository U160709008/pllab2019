package PL_project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;

public class Performance {

	public static void main(String[] args) throws IOException {
		AVLTree avlTree = new AVLTree();
        Heap heap = new Heap(40);
        Scanner input = new Scanner(System.in);
        String file_path1 = input.next();
        String file_path2 = input.next();
		BufferedReader br = new BufferedReader(new FileReader(file_path1));
		BufferedReader br2 = new BufferedReader(new FileReader(file_path2));
		String line = null;

		while ((line = br.readLine()) != null) {
			String[] values = line.split("/n");
			for (String str : values) {
				avlTree.insert(str); 

			}
		}
		
		while ((line = br2.readLine()) != null) {
			String[] values = line.split("/n");
			for (String str2 : values) {
				heap.insert(str2); 

			}
		}
		br.close();
		long start_avl = System.nanoTime();
		System.out.println(avlTree.search("ali"));
		long end_avl = System.nanoTime();
		long elapsed_avl= end_avl-start_avl;
		System.out.println("Execution time in nanoseconds  : " + elapsed_avl);
		
		long start_heap = System.nanoTime();
		System.out.println(heap.search("ali"));
		long end_heap = System.nanoTime();
        long elapsed_heap =end_heap-start_heap;
        System.out.println("Execution time in nanoseconds  : " + elapsed_heap);
	}
}
